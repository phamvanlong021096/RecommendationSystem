import pandas as pd
from research.CF import CF
import numpy as np

# # data file
# r_cols = ['user_id', 'item_id', 'rating']
# ratings = pd.read_csv('ex.dat', sep=' ', names=r_cols, encoding='latin-1')
# Y_data = ratings.as_matrix()
#
# rs = CF(Y_data, k=2, uuCF=0)
# rs.fit()
#
# rs.prinf_recommendation()


r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']

ratings_base = pd.read_csv('data/ub.base', sep='\t', names=r_cols, encoding='latin-1')
ratings_test = pd.read_csv('data/ub.test', sep='\t', names=r_cols, encoding='latin-1')

rate_train = ratings_base.as_matrix()
rate_test = ratings_test.as_matrix()

# indices start from 0
rate_train[:, :2] -= 1
rate_test[:, :2] -= 1

rs = CF(rate_train, k=30, uuCF=1)
rs.fit()

n_test = rate_test.shape[0]
SE = 0  # squared error
for n in range(n_test):
    pred = rs.pred(rate_test[n, 0], rate_test[n, 1], normalized=0)
    SE += (pred - rate_test[n, 2]) ** 2

RMSE = np.sqrt(SE / n_test)
print('User-user CF, RMSE = ', RMSE)

rs = CF(rate_train, k=30, uuCF=0)
rs.fit()

n_test = rate_test.shape[0]
SE = 0
for n in range(n_test):
    pred = rs.pred(rate_test[n, 0], rate_test[n, 1], normalized=0)
    SE += (pred - rate_test[n, 2]) ** 2
RMSE = np.sqrt(SE / n_test)
print('Item-item CF, RMSE = ', RMSE)
