import pandas as pd
from research.MF import MF

r_cols = ['user_id', 'movie_id', 'rating', 'unix_timestamp']


# data move_len da dc sort
ratings_base = pd.read_csv('data/ub.base', sep='\t', names=r_cols, encoding='latin-1')
ratings_test = pd.read_csv('data/ub.test', sep='\t', names=r_cols, encoding='latin-1')

rate_train = ratings_base.as_matrix()
rate_train = sorted(rate_train)
rate_test = ratings_test.as_matrix()

# indices start from 0
rate_train[:, :2] -= 1
rate_test[:, :2] -= 1

rs = MF(rate_train, K=10, lam=.1, print_every=10, learning_rate=0.75, max_iter=100, user_based=1)
rs.fit()
# evaluate on test data
RMSE = rs.evaluate_RMSE(rate_test)
print('User-based MF, RMSE =', RMSE)
