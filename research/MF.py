# Matrix Factorization Collaborative Filtering

import pandas as pd
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy import sparse


class MF(object):

    def __init__(self, Y_data, K, lam=0.1, Xinit=None, Winit=None, learning_rate=0.5,
                 max_iter=1000, print_every=100, user_based=1):
        self.Y_raw_data = Y_data
        self.K = K
        # regularization parameter
        self.lam = lam
        # learning rate for gradient descent
        self.learning_rate = learning_rate
        # maximum number of iterations
        self.max_iter = max_iter
        # print results after print_every iterations
        self.print_every = print_every
        # user-based or item-based
        self.user_based = user_based
        # number of users, items, and ratings. Remember to add 1 since id starts from 0
        self.n_users = int(np.max(Y_data[:, 0])) + 1
        self.n_items = int(np.max(Y_data[:, 1])) + 1
        self.n_ratings = Y_data.shape[0]

        if Xinit is None:  # new
            self.X = np.random.randn(self.n_items, K)
        else:  # or from saved data
            self.X = Xinit

        if Winit is None:
            self.W = np.random.randn(K, self.n_users)
        else:  # from saved data
            self.W = Winit

        # normalized data, update later in normalized_Y function
        self.Y_data_n = self.Y_raw_data.copy()

    def normalize_Y(self):
        if self.user_based:
            user_col = 0
            item_col = 1
            n_objects = self.n_users

        # If we want to normalize based on item, just switch first two columns of data
        else:
            user_col = 1
            item_col = 0
            n_objects = self.n_items

        users = self.Y_raw_data[:, user_col]
        self.mu = np.zeros((n_objects,))
        print("N_object = ", n_objects)
        for n in range(n_objects):
            print("n = ", n)
            # row indices of rating done by user n
            # since indices need to be integers, we need to convert
            ids = np.where(users == n)[0].astype(np.int32)
            # indices of all ratings associated with user n
            item_ids = self.Y_data_n[ids, item_col]
            # and the corresponding ratings
            ratings = self.Y_data_n[ids, 2]
            # take mean
            m = np.mean(ratings)
            if np.isnan(m):
                m = 0  # to avoid empty array and nan value
            self.mu[n] = m
            self.Y_data_n[ids, 2] = ratings - self.mu[n]

    def _normalize_Y_improve(self):
        if self.user_based:
            user_col = 0
            n_objects = self.n_users
        else:
            user_col = 1
            n_objects = self.n_items

        self.mu = np.zeros((n_objects,))
        self.count = np.zeros((n_objects,))
        self.total_rating = np.zeros((n_objects,))
        for row in self.Y_raw_data:
            user = row[user_col]
            rating = row[2]
            self.count[user] += 1
            self.total_rating[user] += rating

        for i in range(n_objects):
            mean = 0
            if self.count[i] != 0:
                mean = self.total_rating[i] / self.count[i]
            self.mu[i] = mean

        for i, row in enumerate(self.Y_raw_data):
            user = row[user_col]
            rating = row[2]
            self.Y_data_n[i, 2] = rating - self.mu[user]

    def prepare(self):
        n = len(self.Y_data_n)
        item_rate_by_user = [[] for i in range(self.n_users)]
        rating_item_rate_by_user = [[] for i in range(self.n_users)]
        user_who_rate_item = [[] for i in range(self.n_items)]
        rating_user_who_rate_item = [[] for i in range(self.n_items)]
        for row in self.Y_data_n:
            user = row[0]
            item = row[1]
            rating = row[2]
            item_rate_by_user[user].append(item)
            rating_item_rate_by_user[user].append(rating)
            user_who_rate_item[item].append(user)
            rating_user_who_rate_item[item].append(rating)

        for i in range(self.n_users):
            item_rate_by_user[i] = np.asarray(item_rate_by_user[i]).astype(np.int32)
            rating_item_rate_by_user[i] = np.asarray(rating_item_rate_by_user[i]).astype(np.int32)

        for i in range(self.n_items):
            user_who_rate_item[i] = np.asarray(user_who_rate_item[i]).astype(np.int32)
            rating_user_who_rate_item[i] = np.asarray(rating_user_who_rate_item[i]).astype(np.int32)

        self.item_rate_by_user = item_rate_by_user
        self.rating_item_rate_by_user = rating_item_rate_by_user
        self.user_who_rate_item = user_who_rate_item
        self.rating_user_who_rate_item = rating_user_who_rate_item

    def loss(self):
        L = 0
        for i in range(self.n_ratings):
            # user, item, rating
            n, m, rate = int(self.Y_data_n[i, 0]), int(self.Y_data_n[i, 1]), self.Y_data_n[i, 2]
            L += 0.5 * (rate - self.X[m, :].dot(self.W[:, n])) ** 2

        # take average
        L /= self.n_ratings
        # regularization, don't ever forget this
        L += 0.5 * self.lam * (np.linalg.norm(self.X, 'fro') + np.linalg.norm(self.W, 'fro'))
        return L

    def get_items_rated_by_user(self, user_id):
        """
        Get all items which are rated by user user_id, and the corresponding ratings
        """
        # ids = np.where(self.Y_data_n[:, 0] == user_id)[0]
        # item_ids = self.Y_data_n[ids, 1].astype(np.int32)  # indices need to be integers
        # ratings = self.Y_data_n[ids, 2]
        # return item_ids, ratings
        return self.item_rate_by_user[user_id], self.rating_item_rate_by_user[user_id]

    def get_users_who_rate_item(self, item_id):
        """
        Get all users who rated item item_id and get the corresponding ratings
        """
        # ids = np.where(self.Y_data_n[:, 1] == item_id)[0]
        # user_ids = self.Y_data_n[ids, 0].astype(np.int32)
        # ratings = self.Y_data_n[ids, 2]
        # return (user_ids, ratings)
        return self.user_who_rate_item[item_id], self.rating_user_who_rate_item[item_id]

    def updateX(self):
        for m in range(self.n_items):
            user_ids, ratings = self.get_users_who_rate_item(m)
            Wm = self.W[:, user_ids]
            # gradient
            grad_xm = -(ratings - self.X[m, :].dot(Wm)).dot(Wm.T) / self.n_ratings + self.lam * self.X[m, :]
            self.X[m, :] -= self.learning_rate * grad_xm.reshape((self.K,))

    def updateW(self):
        for n in range(self.n_users):
            item_ids, ratings = self.get_items_rated_by_user(n)
            Xn = self.X[item_ids, :]
            # gradient
            grad_wn = -Xn.T.dot(ratings - Xn.dot(self.W[:, n])) / self.n_ratings + self.lam * self.W[:, n]
            self.W[:, n] -= self.learning_rate * grad_wn.reshape((self.K,))

    def fit(self):
        self._normalize_Y_improve()
        self.prepare()
        for it in range(self.max_iter):
            self.updateX()
            self.updateW()
            if (it + 1) % self.print_every == 0:
                rmse_train = self.evaluate_RMSE(self.Y_raw_data)
                print('iter = ', it + 1, ', loss =', self.loss(), ', RMSE train =', rmse_train)

    def pred(self, u, i):
        """
        Predict the rating of user u for item i if you need the un
        """
        u = int(u)
        i = int(i)
        if self.user_based:
            bias = self.mu[u]
        else:
            bias = self.mu[i]
        pred = self.X[i, :].dot(self.W[:, u]) + bias
        # truncate if the results are out of range [0, 5]
        if pred < 0:
            return 0
        if pred > 5:
            return 5
        return pred

    def pred_for_user(self, user_id):
        """
        Predict ratings one user give all unrated items
        """
        ids = np.where(self.Y_data_n[:, 0] == user_id)[0]
        items_rated_by_u = self.Y_data_n[ids, 1].tolist()

        y_pred = self.X.dot(self.W[:, user_id]) + self.mu[user_id]
        predicted_ratings = []
        for i in range(self.n_items):
            if i not in items_rated_by_u:
                predicted_ratings.append((i, y_pred[i]))

        return predicted_ratings

    def evaluate_RMSE(self, rate_test):
        n_tests = rate_test.shape[0]
        SE = 0  # squared error
        for n in range(n_tests):
            pred = self.pred(rate_test[n, 0], rate_test[n, 1])
            SE += (pred - rate_test[n, 2]) ** 2

        RMSE = np.sqrt(SE / n_tests)
        return RMSE
